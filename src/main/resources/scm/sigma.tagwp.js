/*
 * Copyright (c) 2010, Sebastian Sdorra
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://bitbucket.org/sdorra/scm-manager
 *
 */

Ext.ns('Sigma.tagwp');

Sigma.tagwp.ConfigPanel = Ext.extend(Sonia.repository.PropertiesFormPanel, {
    formTitleText: 'Tag Write Protection',
    enabledText: 'Enable',
    colTagText: 'Tag',
    colNameText: 'Name',
    colGroupText: 'Is Group',
    colDenyText: 'Deny',
    addText: 'Add',
    removeText: 'Remove',

    addIcon: 'resources/images/add.gif',
    removeIcon: 'resources/images/delete.gif',
    helpIcon: 'resources/images/help.gif',

    enableHelpText: 'Enable Tag write protection. \n\
        Only admins, owners and users defined in the whitelist below are able to write.',
    tagwpGridHelpText: 'Tag write protection hitelist. Deny comes always for allow permissions. \n\
    <b>Note:</b> You can use glob syntax and the placeholders {username} and {mail} for tag names.',

    tagwpStore: null,

    initComponent: function () {
        this.tagwpStore = new Ext.data.ArrayStore({
            root: 'permissions',
            fields: [
                {name: 'tag'},
                {name: 'name'},
                {name: 'group', type: 'boolean'},
                {name: 'deny', type: 'boolean'}
            ],
            sortInfo: {
                field: 'tag'
            }
        });

        this.loadTagwpPermissions(this.tagwpStore, this.item);

        var selectionModel = new Ext.grid.RowSelectionModel({
            singleSelect: true
        });

        var tagwpColModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                editable: true
            },
            columns: [{
                id: 'tag',
                dataIndex: 'tag',
                header: this.colTagText,
                editor: Ext.form.TextField
            }, {
                id: 'name',
                dataIndex: 'name',
                header: this.colNameText,
                editor: Ext.form.TextField
            }, {
                id: 'group',
                dataIndex: 'group',
                xtype: 'checkcolumn',
                header: this.colGroupText,
                width: 40,
                editable: true
            }, {
                id: 'deny',
                dataIndex: 'deny',
                xtype: 'checkcolumn',
                header: this.colDenyText,
                width: 40,
                editable: true
            }]
        });

        var config = {
            title: this.formTitleText,
            items: [{
                xtype: 'checkbox',
                fieldLabel: this.enabledText,
                name: 'tagwpEnabled',
                inputValue: 'true',
                property: 'tagwp.enabled',
                helpText: this.enableHelpText
            }, {
                id: 'tagwpGrid',
                xtype: 'editorgrid',
                clicksToEdit: 1,
                autoExpandColumn: 'tag',
                frame: true,
                width: '100%',
                autoHeight: true,
                autoScroll: false,
                colModel: tagwpColModel,
                sm: selectionModel,
                store: this.tagwpStore,
                viewConfig: {
                    forceFit: true
                },
                tbar: [{
                    text: this.addText,
                    scope: this,
                    icon: this.addIcon,
                    handler: function () {
                        var Permission = this.tagwpStore.recordType;
                        var p = new Permission();
                        var grid = Ext.getCmp('tagwpGrid');
                        grid.stopEditing();
                        this.tagwpStore.insert(0, p);
                        grid.startEditing(0, 0);
                    }
                }, {
                    text: this.removeText,
                    scope: this,
                    icon: this.removeIcon,
                    handler: function () {
                        var grid = Ext.getCmp('tagwpGrid');
                        var selected = grid.getSelectionModel().getSelected();
                        if (selected) {
                            this.tagwpStore.remove(selected);
                        }
                    }
                }, '->', {
                    id: 'tagwpGridHelp',
                    xtype: 'box',
                    autoEl: {
                        tag: 'img',
                        src: this.helpIcon
                    }
                }]
            }]
        };

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        Sigma.tagwp.ConfigPanel.superclass.initComponent.apply(this, arguments);
    },

    afterRender: function () {
        Sigma.tagwp.ConfigPanel.superclass.afterRender.apply(this, arguments);

        Ext.QuickTips.register({
            target: Ext.getCmp('tagwpGridHelp'),
            title: '',
            text: this.tagwpGridHelpText,
            enabled: true
        });
    },

    loadTagwpPermissions: function (store, repository) {
        if (debug) {
            console.debug('load tagwp properties');
        }
        if (!repository.properties) {
            repository.properties = [];
        }
        Ext.each(repository.properties, function (prop) {
            if (prop.key === 'tagwp.permissions') {
                var value = prop.value;
                this.parsePermissions(store, value);
            }
        }, this);
    },

    parsePermissions: function (store, permString) {
        var parts = perString.match(/[^,]+,[^,]+;/g);
        if (debug) {
            console.debug('tag permissions:');
            console.debug(parts);
        }
        Ext.each(parts, function (part) {
            if (part.endsWith(';')) {
                part = part.substring(0, part.length - 1);
            }
            var pa = part.split(',');
            var deny = false;
            if (pa[0].indexOf('!') === 0) {
                deny = true;
                pa[0] = pa[0].substring(1);
            }
            var group = false;
            if (pa[1].indexOf('@') === 0) {
                group = true;
                pa[1] = pa[1].substring(1);
            }
            var Permission = store.recordType;
            var p = new Permission({
                tag: pa[0],
                group: group,
                name: pa[1],
                deny: deny
            });
            if (debug) {
                console.debug('add permission: ');
                console.debug(p);
            }
            store.add(p);
        });
    },

    storeExtraProperties: function (repository) {
        if (debug) {
            console.debug('store tagwp properties');
        }

        // delete old permissions
        Ext.each(repository.properties, function (prop, index) {
            if (prop.key === 'tagwp.permissions') {
                delete repository.properties[index];
            }
        });

        var permissionString = '';
        this.tagwpStore.data.each(function (r) {
            var p = r.data;
            if (p.deny) {
                permissionString += '!';
            }
            permissionString += p.tag + ',';
            if (p.group) {
                permissionString += '@';
            }
            permissionString += p.name + ';';
        });

        if (debug) {
            console.debug('add tagwp permission string: ' + permissionString);
        }

        repository.properties.push({
            key: 'tagwp.permissions',
            value: permissionString
        });
    }

});

Ext.reg('tagwpConfigPanel', Sigma.tagwp.ConfigPanel);

Sonia.repository.openListeners.push(function (repository, panels) {
    if (Sonia.repository.isOwner(repository)) {
        var type = Sonia.repository.getTypeByName(repository.type);

        if (type && type.supportedCommands && type.supportedCommands.indexOf('TAGS') >= 0) {
            panels.push({
                xtype: 'tagwpConfigPanel',
                item: repository
            });
        }
    }
});
