package ru.sigma.scm.tagwp;

import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sonia.scm.group.GroupNames;
import sonia.scm.repository.Changeset;
import sonia.scm.repository.Repository;
import sonia.scm.user.User;
import sonia.scm.util.GlobUtil;

import java.util.List;

public class TagWPContext
{
  /**
   * Field description
   */
  private static final String TAG_HG_DEFAULT = "default";

  /**
   * Field description
   */
  private static final String TYPE_GIT = "git";

  /**
   * Field description
   */
  private static final String TYPE_HG = "hg";

  /**
   * the logger for TagWPContext
   */
  private static final Logger logger =
      LoggerFactory.getLogger(TagWPContext.class);

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs ...
   *
   * @param subject
   * @param user
   * @param repository
   * @param config
   */
  public TagWPContext(Subject subject, User user, Repository repository,
                      TagWPConfiguration config)
  {
    this.subject = subject;
    this.repository = repository;
    this.config = config;
    this.user = user;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Method description
   *
   * @param tag
   * @return
   */
  public boolean isPrivileged(String tag)
  {
    boolean privileged = false;
    String username = user.getName();

    if (!isChangesetDenied(tag)) {
      privileged = isChangesetAllowed(tag);
    }

    if (!privileged) {
      logger.warn("access denied for user {} at tag {}", username, tag);
    }

    return privileged;
  }

  /**
   * Method description
   *
   * @param changeset
   * @return
   */
  public boolean isPrivileged(Changeset changeset)
  {
    boolean privileged;

    String type = repository.getType();

    List<String> tags = changeset.getTags();

    if (tags.isEmpty() && TYPE_GIT.equals(type)) {
      logger.trace(
          "git changeset {} is not the repository head and has no tag information",
          changeset.getId());

      privileged = true;
    } else {
      String tag = getTagName(type, tags);

      privileged = isPrivileged(tag);
    }

    return privileged;
  }

  /**
   * Method description
   *
   * @param type
   * @param tags
   * @return
   */
  private String getTagName(String type, List<String> tags)
  {
    String tag;

    if (tags.isEmpty() && TYPE_HG.equals(type)) {
      tag = TAG_HG_DEFAULT;
    } else {
      tag = tags.get(0);
    }

    return tag;
  }

  /**
   * Method description
   *
   * @return
   */
  private GroupNames getGroups()
  {
    return subject.getPrincipals().oneByType(GroupNames.class);
  }

  /**
   * Method description
   *
   * @param tag
   * @return boolean
   */
  private boolean isChangesetAllowed(String tag)
  {
    boolean allowed = false;

    logger.trace("check allow permissions of user {} for tag {}",
        user.getName(), tag);

    for (TagWPPermission twp : config.getAllowPermissions()) {
      if (isPermissionMatching(twp, tag)) {
        allowed = true;

        break;
      }
    }

    return allowed;
  }

  /**
   * Method description
   *
   * @param tag
   * @return
   */
  private boolean isChangesetDenied(String tag)
  {
    boolean denied = false;

    logger.trace("check deny permissions of user {} for tag {}",
        user.getName(), tag);

    for (TagWPPermission twp : config.getDenyPermissions()) {
      if (isPermissionMatching(twp, tag)) {
        logger.trace("changeset {} denied by {}", twp);
        denied = true;

        break;
      }
    }

    return denied;
  }

  /**
   * Method description
   *
   * @param twp
   * @param tag
   * @return
   */
  private boolean isPermissionMatching(TagWPPermission twp, String tag)
  {
    //J-
    return GlobUtil.matches(twp.getTag(), tag)
        && ((twp.isGroup() && getGroups().contains(twp.getName()))
        || (!twp.isGroup() && user.getName().equals(twp.getName())));
    //J+
  }

  //~--- fields ---------------------------------------------------------------

  /**
   * Field description
   */
  private final TagWPConfiguration config;

  /**
   * Field description
   */
  private final Repository repository;

  /**
   * Field description
   */
  private final Subject subject;

  /**
   * Field description
   */
  private final User user;
}
