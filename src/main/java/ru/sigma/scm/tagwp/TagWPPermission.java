package ru.sigma.scm.tagwp;

import com.google.common.base.Objects;

import java.io.Serializable;

public class TagWPPermission implements Serializable
{

  private static final long serialVersionUID = -8545699083970087133L;

  public static enum Type
  {
    ALLOW, DENY;
  }

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs ...
   *
   * @param tag
   * @param name
   * @param group
   */
  public TagWPPermission(String tag, String name, boolean group)
  {
    this(tag, name, group, Type.ALLOW);
  }

  /**
   * Constructs ...
   *
   * @param tag
   * @param name
   * @param group
   * @param type
   */
  public TagWPPermission(String tag, String name, boolean group,
                         Type type)
  {
    this(tag, tag, name, group, type);
  }

  /**
   * Constructs ...
   *
   * @param tagPattern
   * @param tag
   * @param name
   * @param group
   * @param type
   */
  public TagWPPermission(String tagPattern, String tag, String name,
                         boolean group, Type type)
  {
    this.tagPattern = tagPattern;
    this.tag = tag;
    this.name = name;
    this.group = group;
    this.type = type;
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Method description
   *
   * @param obj
   * @return
   */
  @Override
  public boolean equals(Object obj)
  {
    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    final TagWPPermission other = (TagWPPermission) obj;

    //J-
    return Objects.equal(tagPattern, other.tagPattern)
        && Objects.equal(tag, other.tag)
        && Objects.equal(name, other.name)
        && Objects.equal(group, other.group)
        && Objects.equal(type, other.type);
    //J+
  }

  /**
   * Method description
   *
   * @return
   */
  @Override
  public int hashCode()
  {
    return Objects.hashCode(tagPattern, tag, name, group, type);
  }

  /**
   * Method description
   *
   * @return
   */
  @Override
  public String toString()
  {
    //J-
    return Objects.toStringHelper(this)
        .add("tagPattern", tagPattern)
        .add("tag", tag)
        .add("name", name)
        .add("group", group)
        .add("type", type)
        .toString();
    //J+
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Method description
   *
   * @return
   */
  public String getTag()
  {
    return tag;
  }

  /**
   * Method description
   *
   * @return
   */
  public String getTagPattern()
  {
    return tagPattern;
  }

  /**
   * Method description
   *
   * @return
   */
  public String getName()
  {
    return name;
  }

  /**
   * Method description
   *
   * @return
   */
  public Type getType()
  {
    return type;
  }

  /**
   * Method description
   *
   * @return
   */
  public boolean isGroup()
  {
    return group;
  }

  //~--- fields ---------------------------------------------------------------

  /**
   * Field description
   */
  private final String tag;

  /**
   * Field description
   */
  private final String tagPattern;

  /**
   * Field description
   */
  private final boolean group;

  /**
   * Field description
   */
  private final String name;

  /**
   * Field description
   */
  private final Type type;
}
