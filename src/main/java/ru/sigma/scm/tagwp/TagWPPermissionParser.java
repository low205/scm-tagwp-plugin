package ru.sigma.scm.tagwp;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sonia.scm.user.User;

import java.util.Iterator;
import java.util.Set;

import static ru.sigma.scm.tagwp.TagWPPermission.Type;

public class TagWPPermissionParser
{
  /**
   * Field description
   */
  public static final String VAR_MAIL = "\\{mail\\}";

  /**
   * Field description
   */
  public static final String VAR_USERNAME = "\\{username\\}";

  /**
   * the logger for TagWPPermissionParser
   */
  private static final Logger logger =
      LoggerFactory.getLogger(TagWPPermissionParser.class);

  //~--- methods --------------------------------------------------------------

  /**
   * Method description
   *
   * @param permissions
   * @param property
   * @param user
   */
  public static void parse(Set<TagWPPermission> permissions, String property, User user)
  {
    parse(permissions, permissions, property, user);
  }

  /**
   * Method description
   *
   * @param allowPermissions
   * @param denyPermissions
   * @param property
   * @param user
   */
  public static void parse(Set<TagWPPermission> allowPermissions,
                           Set<TagWPPermission> denyPermissions, String property, User user)
  {
    logger.trace("try to parse permissions string {}", property);

    Iterable<String> permissionStrings =
        Splitter.on(";").omitEmptyStrings().trimResults().split(property);

    for (String permissionString : permissionStrings) {
      logger.trace("try to parse permission string {}", permissionString);

      TagWPPermission permission = parsePermission(user, permissionString);

      if (permission != null) {
        logger.debug("append tagwp permission {}", permission);

        if (permission.getType() == Type.DENY) {
          denyPermissions.add(permission);
        } else {
          allowPermissions.add(permission);
        }
      } else {
        logger.warn("failed to parse permission string {}", permissionString);
      }
    }
  }

  /**
   * Method description
   *
   * @param user
   * @param permissionString
   * @return
   */
  @VisibleForTesting
  static TagWPPermission parsePermission(User user, String permissionString)
  {
    TagWPPermission permission = null;
    Iterator<String> parts = Splitter.on(
        ",").omitEmptyStrings().trimResults().split(
        permissionString).iterator();

    if (parts.hasNext()) {
      Type type = Type.ALLOW;
      String tagPattern = parts.next();

      if (tagPattern.startsWith("!")) {
        type = Type.DENY;
        tagPattern = tagPattern.substring(1);
      }

      String tag = tagPattern;

      if (user != null) {
        //J-
        tag = tagPattern.replaceAll(
            VAR_USERNAME, Strings.nullToEmpty(user.getName())
        ).replaceAll(
            VAR_MAIL, Strings.nullToEmpty(user.getMail())
        );
        //J+
      }

      if (parts.hasNext()) {

        String name = parts.next();
        boolean group = false;

        if (name.startsWith("@")) {
          group = true;
          name = name.substring(1);
        }

        permission = new TagWPPermission(tagPattern, tag, name, group,
            type);
      }
    }

    return permission;
  }
}
