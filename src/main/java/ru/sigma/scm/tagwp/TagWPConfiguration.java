package ru.sigma.scm.tagwp;

import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sonia.scm.PropertiesAware;
import sonia.scm.user.User;

import java.io.Serializable;
import java.util.Set;

public class TagWPConfiguration implements Serializable
{

  private static final long serialVersionUID = -2587625428812977793L;

  /**
   * Field description
   */
  static final String PROPERTY_ENABLED = "tagwp.enabled";

  /**
   * Field description
   */
  static final String PROPERTY_PERMISSIONS = "tagwp.permissions";

  /**
   * the logger for TagWPConfiguration
   */
  private static final Logger logger =
      LoggerFactory.getLogger(TagWPConfiguration.class);

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs ...
   *
   * @param properties
   */
  public TagWPConfiguration(PropertiesAware properties)
  {
    this(properties, null);
  }

  /**
   * Constructs ...
   *
   * @param properties
   * @param user
   */
  public TagWPConfiguration(PropertiesAware properties, User user)
  {
    this.properties = properties;
    this.user = user;
    enabled = Boolean.valueOf(properties.getProperty(PROPERTY_ENABLED));
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Method description
   *
   * @return
   */
  public Set<TagWPPermission> getAllowPermissions()
  {
    if (allowPermissions == null) {
      parsePermissions();
    }

    return allowPermissions;
  }

  /**
   * Method description
   *
   * @return
   */
  public Set<TagWPPermission> getDenyPermissions()
  {
    if (denyPermissions == null) {
      parsePermissions();
    }

    return denyPermissions;
  }

  /**
   * Method description
   *
   * @return
   */
  public boolean isEnabled()
  {
    return enabled;
  }

  /**
   * Method description
   *
   * @return
   */
  public boolean isPermissionConfigEmpty()
  {
    if ((allowPermissions == null) || (denyPermissions == null)) {
      parsePermissions();
    }

    return allowPermissions.isEmpty() && denyPermissions.isEmpty();
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Method description
   */
  private void parsePermissions()
  {
    allowPermissions = Sets.newHashSet();
    denyPermissions = Sets.newHashSet();

    String property = properties.getProperty(PROPERTY_PERMISSIONS);

    if (!Strings.isNullOrEmpty(property)) {
      TagWPPermissionParser.parse(allowPermissions, denyPermissions,
          property, user);
    } else if (logger.isDebugEnabled()) {
      logger.debug("no permissions found");
    }
  }

  //~--- fields ---------------------------------------------------------------

  /**
   * Field description
   */
  private final PropertiesAware properties;

  /**
   * Field description
   */
  private final User user;

  /**
   * Field description
   */
  private Set<TagWPPermission> allowPermissions;

  /**
   * Field description
   */
  private Set<TagWPPermission> denyPermissions;

  /**
   * Field description
   */
  private boolean enabled = false;

}
