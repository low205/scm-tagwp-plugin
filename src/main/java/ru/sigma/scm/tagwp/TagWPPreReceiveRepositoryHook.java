package ru.sigma.scm.tagwp;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Joiner;
import com.google.common.eventbus.Subscribe;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sonia.scm.EagerSingleton;
import sonia.scm.event.Subscriber;
import sonia.scm.plugin.ext.Extension;
import sonia.scm.repository.Changeset;
import sonia.scm.repository.PermissionType;
import sonia.scm.repository.PreReceiveRepositoryHookEvent;
import sonia.scm.repository.Repository;
import sonia.scm.repository.api.HookContext;
import sonia.scm.repository.api.HookFeature;
import sonia.scm.repository.api.HookTagProvider;
import sonia.scm.security.RepositoryPermission;
import sonia.scm.user.User;

@Extension
@EagerSingleton
@Subscriber(async = false)
public class TagWPPreReceiveRepositoryHook
{
  /**
   * the logger for TagwpPreReceiveRepositoryHook
   */
  private static final Logger logger =
      LoggerFactory.getLogger(TagWPPreReceiveRepositoryHook.class);

  //~--- methods --------------------------------------------------------------

  /**
   * Method description
   *
   * @param event
   */
  @Subscribe
  public void onEvent(PreReceiveRepositoryHookEvent event)
  {
    Repository repository = event.getRepository();

    if (repository != null) {
      logger.trace("received hook for repository {}", repository.getName());

      Subject subject = getSubject();

      logger.trace("check tagwp for user {} and repository {}",
          subject.getPrincipal(), repository.getName());

      if (!subject.isPermitted(owner(repository))) {
        User user = subject.getPrincipals().oneByType(User.class);
        TagWPConfiguration config = new TagWPConfiguration(repository,
            user);

        if (config.isEnabled()) {
          logger.debug("tagwp is enabled for repository {}",
              repository.getName());

          handleTagWP(subject, user, config, event);

        } else {
          logger.debug("tagwp is disabled for repository {}",
              repository.getName());
        }

      } else {
        logger.debug(
            "skip user {}, because the user has owner permissions for repository {}",
            subject.getPrincipal(), repository.getName());
      }
    } else {
      logger.warn("received hook without repository");
    }

  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Method description
   *
   * @return
   */
  @VisibleForTesting
  protected Subject getSubject()
  {
    return SecurityUtils.getSubject();
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Method description
   *
   * @param ctx
   * @param tag
   */
  private void checkTag(TagWPContext ctx, String tag)
  {
    if (!ctx.isPrivileged(tag)) {
      logger.warn("access denied for tag {}", tag);

      throw new TagWPException("no write permissions for the tag ".concat(tag));
    }
  }

  /**
   * Method description
   *
   * @param ctx
   * @param tagProvider
   */
  private void checkTagProvider(TagWPContext ctx,
                                HookTagProvider tagProvider)
  {
    for (String tag : tagProvider.getCreatedOrModified()) {
      checkTag(ctx, tag);
    }

    for (String tag : tagProvider.getDeletedOrClosed()) {
      checkTag(ctx, tag);
    }
  }

  /**
   * Method description
   *
   * @param ctx
   * @param changesets
   */
  private void checkChangesets(TagWPContext ctx,
                               Iterable<Changeset> changesets)
  {
    for (Changeset changeset : changesets) {
      if (!ctx.isPrivileged(changeset)) {
        logger.warn("access denied for tag {}", changeset.getTags());

        throw new TagWPException(
            "no write permissions for one of the following tags: ".concat(
                Joiner.on(", ").join(changeset.getTags())
            )
        );
      }
    }
  }

  /**
   * Method description
   *
   * @param ctx
   * @param event
   * @param hookCtx
   */
  private void checkWithHookContext(TagWPContext ctx,
                                    PreReceiveRepositoryHookEvent event, HookContext hookCtx)
  {
    if (hookCtx.isFeatureSupported(HookFeature.TAG_PROVIDER)) {
      logger.trace("use hook tag provider to check permissions");
      checkTagProvider(ctx, hookCtx.getTagProvider());
    } else if (hookCtx.isFeatureSupported(HookFeature.CHANGESET_PROVIDER)) {
      logger.trace("use hook changeset provider to check permissions");
      checkChangesets(ctx, hookCtx.getChangesetProvider().getChangesets());
    } else {
      logger.trace("use changesets provided by event to check permissions");
      checkChangesets(ctx, event.getChangesets());
    }
  }

  /**
   * Method description
   *
   * @param subject
   * @param user
   * @param config
   * @param event
   */
  private void handleTagWP(Subject subject, User user,
                           TagWPConfiguration config, PreReceiveRepositoryHookEvent event)
  {
    if (!config.isPermissionConfigEmpty()) {
      Repository repository = event.getRepository();

      TagWPContext ctx = new TagWPContext(subject, user, repository,
          config);

      if (event.isContextAvailable()) {
        checkWithHookContext(ctx, event, event.getContext());
      } else {
        logger.trace("use changesets provided by event to check permissions");
        checkChangesets(ctx, event.getChangesets());
      }

    } else {
      logger.warn("tagwp permissions are empty, access denied");

      throw new TagWPException("no tagwp permissions defined");
    }
  }

  /**
   * Method description
   *
   * @param repository
   * @return
   */
  private RepositoryPermission owner(Repository repository)
  {
    return new RepositoryPermission(repository, PermissionType.OWNER);
  }
}
