package ru.sigma.scm.tagwp;

public class TagWPException extends RuntimeException
{

  private static final long serialVersionUID = 6981011607284028457L;

  public TagWPException(String message)
  {
    super(message);
  }
}
